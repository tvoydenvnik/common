<?php

namespace Tvoydenvnik\Common\Tests;


use Tvoydenvnik\Common\Lib\RequireJs;

class RequireJsTest extends \PHPUnit_Framework_TestCase
{
    public function testAddJquery()
    {
        $req = new RequireJs();

        $req->addDependence("jquery", "$");
        $req->addDependence("jquery", "$");

        $code = <<<EOD
requirejs(['jquery'], function($){

});
EOD;
        $this->assertEquals($code, $req->getCode());
    }

    public function testAddJqueryWithCode()
    {
        $req = new RequireJs();

        $req->addDependence("jquery", "$")
            ->addJsCode("window.$ = $;")
            ->addJsCode("alert(1);");

        $code = <<<EOD
requirejs(['jquery'], function($){
window.$ = $;
alert(1);
});
EOD;
        $this->assertEquals($code, $req->getCode());
    }


    public function testAddAppWithStartApp()
    {
        $req = new RequireJs();

        $req->addDependence("app", "myApp")
            ->addJsCodeCallFunction("myApp", array("someParams"=>array(1, 2, 3)))
            ->addJsCode("alert(1);");

        $code = <<<EOD
requirejs(['app'], function(myApp){
myApp.startApp({"someParams":[1,2,3]});
alert(1);
});
EOD;
        $this->assertEquals($code, $req->getCode());
    }

}