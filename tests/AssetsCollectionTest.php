<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 09.08.2016
 * Time: 12:09
 */

namespace Tvoydenvnik\Common\Tests;


use Tvoydenvnik\Common\Lib\AssetsCollection;

class AssetsCollectionTest extends \PHPUnit_Framework_TestCase
{

    public function testAddJs()
    {
        $assets = new AssetsCollection("http://baseUrl", "v1.1", array(
            "requirejs"=>"requirePath/file.js",
            "requirejsConfig"=>"requireConfigPath/file.js",
        ), "=1");

        $assets->addJs("test", "path/path2/file.js");
        $assets->addJs("test", "path/path3/file.js");
        $assets->addJs("test2", "path/path2/file.js");


        $code = <<<EOD
<!-- Assets.test:Js  -->
<script src="http://baseUrl/v1.1/path/path2/file.js=1"></script>
<script src="http://baseUrl/v1.1/path/path3/file.js=1"></script>
EOD;
        $this->assertEquals($code, $assets->getCode("test"));

        $code = <<<EOD
<!-- Assets.test2:Js  -->
<script src="http://baseUrl/v1.1/path/path2/file.js=1"></script>
EOD;
        $this->assertEquals($code, $assets->getCode("test2"));
    }


    public function testAddInlineJs()
    {
        $assets = new AssetsCollection("http://baseUrl", "v1.1", array(             "requirejs"=>"requirePath/file.js",             "requirejsConfig"=>"requireConfigPath/file.js",         ), "=1");

        $assets->addInlineJs("test", "alert(1)");
        $assets->addInlineJs("test", "alert(2)");


        $code = <<<EOD
<!-- Assets.test:InlineJs  -->
<script type="text/javascript">
alert(1)
alert(2)
</script>
EOD;
        $this->assertEquals($code, $assets->getCode("test"));


    }

    public function testAddCss()
    {
        $assets = new AssetsCollection("http://baseUrl", "v1.1", array(             "requirejs"=>"requirePath/file.js",             "requirejsConfig"=>"requireConfigPath/file.js",         ), "=1");

        $assets->addCss("test", "my/css.js");
        $assets->addCss("test", "http://my/css.js", true);


        $code = <<<EOD
<!-- Assets.test:css  -->
<link href="http://baseUrl/v1.1/my/css.js=1" rel="stylesheet" type="text/css">
<link href="http://my/css.js" rel="stylesheet" type="text/css">
EOD;
        $this->assertEquals($code, $assets->getCode("test"));


    }


    public function testAddRequireJs()
    {
        $assets = new AssetsCollection("http://baseUrl", "v1.1", array("requirejs"=>"requirePath/file.js", "requirejsConfig"=>"requireConfigPath/file.js"), "=1");

        $assets->addRequireJs();
        $assets->requirejs->addJquery();


        $code = <<<EOD
<!-- Assets.requirejs:Js  -->
<script src="http://baseUrl/v1.1/requirePath/file.js=1"></script>
<script src="http://baseUrl/v1.1/requireConfigPath/file.js=1"></script>
<!-- Assets.requirejs:RequireJs  -->
<script type="text/javascript">
requirejs(['jquery'], function(jquery){

});
</script>
EOD;
        $this->assertEquals($code, $assets->getCodeRequireJs());


    }


    public function testAddInlineJsBefore()
    {
        $assets = new AssetsCollection("http://baseUrl", "v1.1", array(             "requirejs"=>"requirePath/file.js",             "requirejsConfig"=>"requireConfigPath/file.js",         ), "=1");

        $assets->addInlineJs("test", "alert(2)");
        $assets->addInlineJsBeforeJs("test", "alert(1)");
        $assets->addJs("test", 'test.js');


        $code = <<<EOD
<!-- Assets.test:InlineJsBeforeJs  -->
<script type="text/javascript">
alert(1)
</script>
<!-- Assets.test:Js  -->
<script src="http://baseUrl/v1.1/test.js=1"></script>
<!-- Assets.test:InlineJs  -->
<script type="text/javascript">
alert(2)
</script>
EOD;
        $this->assertEquals($code, $assets->getCode("test"));


    }
}