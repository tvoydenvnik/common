<?php
/*
 * changelog
 * 2016-08-09 - Создан класс.
 */

namespace Tvoydenvnik\Common\Lib;

/**
 * Класс используется для загрузки модулей с помощью require.js
 * Class RequireJs
 * @package Tvoydenvnik\Common\Lib
 */

class RequireJs
{

    //private static $_instance = null;

    private $dependence = array();
    private $jscode = array();
    private $dependenceCache = array();

    /**
     * Добавляем зависимость для загрузки requirejs
     * @param $sPath - Путь к модулю
     * @param $sName - Наименование в callback переменной
     * @return RequireJs
     */
    public function addDependence($sPath, $sName)
    {

        if(isset($this->dependenceCache[$sName])){
            return $this;
        }
        array_push($this->dependence, array(
            'path'=>$sPath,
            'name'=>$sName
        ));
        $this->dependenceCache[$sName] = true;

        return $this;

    }

    public function addUikitJs(){
        return $this->addDependence('uikit','uikit');
    }

    public function addJquery(){
        return $this->addDependence('jquery','jquery');
    }

    public function addJsCodeCallFunction($sAppName, $arParams = null, $sCallFunction = "startApp"){

        $sParams = '';
        if(is_array($arParams)){
            $sParams = json_encode($arParams);
        }
        $code = $sAppName . "." . $sCallFunction . '('.$sParams.');';

        $this->addJsCode($code);

        return $this;
    }

    /**
     * Добавляем js код в внутрь requirejs callback функции
     * @param $sCode
     * @return RequireJs
     */
    public function addJsCode($sCode)
    {
        array_push($this->jscode, $sCode);
        return $this;
    }

    /**
     * Возвращает код для вставки в script тег
     * @return string
     */
    public function getCode(){

        $deps = array();
        $vars = array();

        for ($i=0,$length=count($this->dependence);$i<$length;$i++){
            $item = $this->dependence[$i];
            array_push($deps, "'" . $item['path'] . "'");
            array_push($vars, $item['name']);
        }

        $depsStr = implode(",", $deps);
        $varsStr = implode(",", $vars);
        $jscode = implode("\r\n", $this->jscode);

        $code = <<<EOD
requirejs([$depsStr], function($varsStr){
$jscode
});
EOD;
        return $code;


    }


//    public static function get(){
//
//        if(self::$_instance == null){
//            self::$_instance = new RequireJs();
//        }
//
//        return self::$_instance;
//    }
}