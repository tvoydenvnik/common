<?php

namespace Tvoydenvnik\Common\Lib;
use Phalcon\Di\FactoryDefault;

/**
 * Class Log
 * @package classes
 */
class Log {

    public static function add($pLogName, $pMassage, $pDateSeparate = true){
        
        
        $l_file_name = FactoryDefault::getDefault()->get("config")->log->path .'/'.$pLogName;
        if($pDateSeparate == true){
            $l_date = new \DateTime('now');
            $l_file_name = $l_file_name .'__'. $l_date->format('Y_m_d');
        }
        $l_file_name = $l_file_name . '.txt';
        $fLog = fopen($l_file_name,'a');
        if(is_array($pMassage) == true){
            if(isset($pMassage['time'])==false){
                $l_date_now = new \DateTime('now');//
                $pMassage['time'] = $l_date_now->format('Y-m-d H:i:s');
            }
        }
        if(is_array($pMassage) == true || is_object($pMassage) == true){
            $pMassage = JSON::json_encode($pMassage);
        }else{
            $l_date_now = new \DateTime('now');
            $pMassage = $l_date_now->format('Y-m-d H:i:s').' :: '.$pMassage;
        }
        fwrite($fLog, $pMassage ."\n");
        fclose($fLog);
    }

}

