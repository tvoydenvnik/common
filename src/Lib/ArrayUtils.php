<?php

namespace Tvoydenvnik\Common\Lib;


class ArrayUtils {


    public static function getNumericUniqueArray(array $arArray)
    {

        $arArrayNew = array();
        foreach ($arArray as $key => $val) {
            if (intval($val) > 0) {
                array_push($arArrayNew, intval($val));
            }

        }
        return array_values(array_unique($arArrayNew));

    }
}