<?php

namespace Tvoydenvnik\Common\Lib;


class MzrConfigForJsApp
{

    private $config = array();

    private $useWebpack = false;
    private $requirejs = array();
    private $prices = array();
    private $versions = array();
    private $platformId = 0;
    private $appUrl = "";
    private $account = array();
    private $endPoints = array();
    private $partnerParams = array();
    private $routeHistory = "browser";
    private $projectMenu = array();
    private $loadCss = array();
    private $oldParams = array();
    private $supportEmail = "";

    public function __construct($config)
    {
        $this->config = $config;

        $dConfig = new DotNotation($this->config);
        $this->useWebpack = $dConfig->get('useWebpack', false);
//        $this->baseUrl = $dConfig->get('baseUrl', "");
//        if(substr($this->baseUrl,-1) != '/'){
//            $this->baseUrl = $this->baseUrl . '/';
//        }
        $this->requirejs = $dConfig->get('requirejs', array(
            "baseUrl"=>"",
            "urlArgs"=>""
        ));
        $this->prices = $dConfig->get('prices', array());
        $this->versions = $dConfig->get('versions', array());
        $this->platformId = $dConfig->get('platformId', 101);
        $this->appUrl = $dConfig->get('appUrl', "");
        $this->account = $dConfig->get('account', array(
            "isAuth"=>false,
            "id"=>0,
            "name"=>"Гость",
            "avatar"=>"",
            "roles"=>array("guest")
        ));

        $this->endPoints = $dConfig->get('endPoints', array());
        $this->partnerParams = $dConfig->get('partnerParams', array());
        $this->loadCss = $dConfig->get('loadCss', array());
        $this->oldParams = $dConfig->get('oldParams', array());
        $this->supportEmail = $dConfig->get('supportEmail', "support@health-diet.ru");
        $this->routeHistory = $dConfig->get('routeHistory', "browser");


    }

    private function create(){

        $mzrConfig = array(

            "useWebpack"=> $this->useWebpack,
            "requirejs"=> $this->requirejs,
            "prices"=> $this->prices,
            "versions"=> $this->versions,
            "platformId"=> $this->platformId,
            "appUrl"=> $this->appUrl,
            "account"=> $this->account,
            "endPoints"=> $this->endPoints,
            "partnerParams"=> $this->partnerParams,
            "projectMenu"=> $this->projectMenu,
            "loadCss"=> $this->loadCss,
            "oldParams"=> $this->oldParams,
            "supportEmail"=> $this->supportEmail,
            "routeHistory"=>$this->routeHistory
        );

        return $mzrConfig;
    }

    public function createAsJs(){

        $option = 0;
        if(isset($this->config["_configPretyPrint"]) && $this->config["_configPrettyPrint"] == true){
            $option = JSON_PRETTY_PRINT;
        }

        return "window.mzrConfig = " .json_encode($this->create(), $option) .';';
    }
}