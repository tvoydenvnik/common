<?php
/*
 * changelog
 * 2016-08-09 - Создан класс.
 */

namespace Tvoydenvnik\Common\Lib;

class AssetsCollection
{

    private $assets = array();
    private $baseUrl = '';
    private $appVersion = '';
    private $urlArgs = '';

    /*
     * Основные коллекции
     */
    public static $cCOLLECTION_HEADER = 'header';
    public static $cCOLLECTION_REQUIREJS = 'requirejs';
    public static $cCOLLECTION_FOOTER = 'footer';

    /**
     * Пути к различным скриптам
     * @var array
     */
    private $commonPath = array();

    /**
     * @var RequireJs
     */
    public $requirejs = null;


    public function __construct($baseUrl, $appVersion, $commonPath, $urlArgs = "")
    {
        $this->baseUrl = $baseUrl;
        $this->appVersion = $appVersion;
        $this->urlArgs = $urlArgs;
        $this->commonPath = $commonPath;
        $this->requirejs = new RequireJs();
    }


    public function addJs($sCollection, $sPath, $bIsExternal = false){
       $this->addAsset('js', $sCollection, $sPath, $bIsExternal);
       return $this;
    }

    /**
     * Инлайн код, который будет подключен после загружаемых скриптов
     * @param $sCollection
     * @param $sCode
     * @return $this
     */
    public function addInlineJs($sCollection, $sCode){
        $this->addCollection($sCollection);

        array_push($this->assets[$sCollection], array(
            'code'=>$sCode,
            'type'=>'inlineJs'
        ));
        return $this;
    }

    /**
     * Инлайн код, который будет подключен перед загружаемых скриптов
     * @param $sCollection
     * @param $sCode
     * @return $this
     */
    public function addInlineJsBeforeJs($sCollection, $sCode){
        $this->addCollection($sCollection);

        array_push($this->assets[$sCollection], array(
            'code'=>$sCode,
            'type'=>'inlineJsBeforeJs'
        ));
        return $this;
    }


    public function addCss($sCollection, $sPath, $bIsExternal = false){
        $this->addAsset('css', $sCollection, $sPath, $bIsExternal);
        return $this;
    }

    /**
     * Добавим на страницу require.js и его конфиг
     * @return $this
     */
    public function addRequireJs(){

        $this->addJs(self::$cCOLLECTION_REQUIREJS, $this->getPathFromCommon("requirejs"))
            ->addJs(self::$cCOLLECTION_REQUIREJS, $this->getPathFromCommon("requirejsConfig"));

        return $this;
    }

    public function getCode($sCollection){

        $inlineJs = array();
        $inlineJsBefore = array();
        $js = array();
        $css = array();

        if(isset($this->assets[$sCollection])==false || is_array($this->assets[$sCollection])==false){
            return '';
        }

        foreach ($this->assets[$sCollection] as $asset){
            if($asset['type'] == 'css'){

                array_push($css, '<link href="'. $asset['path'] .'" rel="stylesheet" type="text/css">');

            }elseif ($asset['type'] == 'js'){

                array_push($js, '<script src="'.$asset['path'].'"></script>');

            }elseif ($asset['type'] == 'inlineJs'){

                array_push($inlineJs, $asset['code']);

            }elseif ($asset['type'] == 'inlineJsBeforeJs'){

                array_push($inlineJsBefore, $asset['code']);

            }

        }

        $lResult = "";
        if(count($css)>0){
            if($lResult!=""){
                $lResult = $lResult . "\r\n";
            }
            $lResult = $lResult ."<!-- Assets.$sCollection:css  -->". "\r\n";

            $lResult = $lResult . implode("\r\n", $css);
        }

        if(count($inlineJsBefore)>0){
            $inline =  implode("\r\n", $inlineJsBefore);
            $code = <<<EOD
<script type="text/javascript">
$inline
</script>
EOD;

            if($lResult!=""){
                $lResult = $lResult . "\r\n";
            }
            $lResult = $lResult ."<!-- Assets.$sCollection:InlineJsBeforeJs  -->". "\r\n";
            $lResult = $lResult . $code;
        }

        if(count($js)>0){
            if($lResult!=""){
                $lResult = $lResult . "\r\n";
            }
            $lResult = $lResult . "<!-- Assets.$sCollection:Js  -->". "\r\n";
            $lResult = $lResult . implode("\r\n", $js);
        }

        if(count($inlineJs)>0){
            $inline =  implode("\r\n", $inlineJs);
            $code = <<<EOD
<script type="text/javascript">
$inline
</script>
EOD;

            if($lResult!=""){
                $lResult = $lResult . "\r\n";
            }
            $lResult = $lResult ."<!-- Assets.$sCollection:InlineJs  -->". "\r\n";
            $lResult = $lResult . $code;
        }

        //если это RequireJs коллекция, то вывдем его код
        if($sCollection == self::$cCOLLECTION_REQUIREJS){
            if($lResult!=""){
                $lResult = $lResult . "\r\n";
            }

            $codeRequire =$this->requirejs->getCode();
            $codeRequireJsWrapper = <<<EOD
<!-- Assets.$sCollection:RequireJs  -->
<script type="text/javascript">
$codeRequire
</script>
EOD;

            $lResult = $lResult .  $codeRequireJsWrapper;
        }

        return $lResult;

    }

    public function getCodeRequireJs(){
        return $this->getCode(self::$cCOLLECTION_REQUIREJS);
    }

    public function addUikitCss(){
        $this->addCss(self::$cCOLLECTION_HEADER, $this->getPathFromCommon("uikitcss"));
        return $this;
    }


    public function addYandexShare(){
        $this->addJs(self::$cCOLLECTION_FOOTER, $this->getPathFromCommon("es5-shims"), true);
        $this->addJs(self::$cCOLLECTION_FOOTER, $this->getPathFromCommon("yandex-share2"), true);
        return $this;
    }

    public function addSwfobject(){
        $this->addJs(self::$cCOLLECTION_FOOTER, $this->getPathFromCommon("swfobject"), true);
        return $this;
    }


    private function addCollection($sCollection){
        if(isset($this->assets[$sCollection])==false || is_array($this->assets[$sCollection]) == false){
            $this->assets[$sCollection] = array();
        }
    }

    private function addAsset($sType, $sCollection, $sPath, $bIsExternal = false){

        $this->addCollection($sCollection);

        array_push($this->assets[$sCollection], array(
            'path'=>($bIsExternal == false? $this->getAppPath() . $sPath . $this->urlArgs : $sPath),
            'type'=>$sType
        ));
    }

    private function getPathFromCommon($sName){
        if(isset($this->commonPath[$sName])){
            return $this->commonPath[$sName];
        }

        return '';
    }
    private function getAppPath(){

        if($this->appVersion == '' && $this->baseUrl == '') {

            return '';

        }else if($this->appVersion == ''){

            return $this->baseUrl . '/';

        }else{

            return $this->baseUrl . '/' . $this->appVersion . '/';

        }

    }

}