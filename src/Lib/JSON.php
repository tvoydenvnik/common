<?php
namespace Tvoydenvnik\Common\Lib;
class JSON {


    /*
     * fix
     * По какой то причине появляется символ 239 в самом начале файла, при выгрузке файлов из 1с
     * Которой не дает работать функции json_decode
     */
    public static function file_get_contents($fn) {
        $content = file_get_contents($fn);
        if($content == false){
            return false;
        }

        if(ord($content) == 239){
            $content = substr($content, 1);
        }
        return $content;
    }


    public static function json_from_file($sPath){

        if(file_exists($sPath) == false){
            return false;
        }

        $file = self::file_get_contents($sPath);
        if($file === false){
            return false;
        }else{

            try{
                return json_decode($file, true);
            }catch (\Exception $e){
                return false;
            }
        }
    }

    //решает проблему с русскими символами
    public static function json_encode($pValue){
        try{
            if(count($pValue)==0){
                return '';
            }


            $lResult = json_encode( $pValue);

            //проблема кирилицы
            $arr_replace_utf = array('\u0410', '\u0430','\u0411','\u0431','\u0412','\u0432',

                '\u0413','\u0433','\u0414','\u0434','\u0415','\u0435','\u0401','\u0451','\u0416',

                '\u0436','\u0417','\u0437','\u0418','\u0438','\u0419','\u0439','\u041a','\u043a',

                '\u041b','\u043b','\u041c','\u043c','\u041d','\u043d','\u041e','\u043e','\u041f',

                '\u043f','\u0420','\u0440','\u0421','\u0441','\u0422','\u0442','\u0423','\u0443',

                '\u0424','\u0444','\u0425','\u0445','\u0426','\u0446','\u0427','\u0447','\u0428',

                '\u0448','\u0429','\u0449','\u042a','\u044a','\u042b','\u044b','\u042c','\u044c',

                '\u042d','\u044d','\u042e','\u044e','\u042f','\u044f');

            $arr_replace_cyr = array('А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е',

                'Ё', 'ё', 'Ж','ж','З','з','И','и','Й','й','К','к','Л','л','М','м','Н','н','О','о',

                'П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ч','ч','Ш','ш',

                'Щ','щ','Ъ','ъ','Ы','ы','Ь','ь','Э','э','Ю','ю','Я','я');

            $lResult = str_replace($arr_replace_utf,$arr_replace_cyr,$lResult);

            return $lResult;

        }
        catch (\Exception $e)
        {
            return '';
        }
    }
    public static function json_decode($json, $assoc = true, $on_error_return_null = false){
        if(trim($json) == '' || $json == null){
            return array();
        }
        try{
            return json_decode(trim($json), $assoc);
        }
        catch (\Exception $e)
        {
            if($on_error_return_null == true){
                return null;
            }
            return array();
        }

    }
}