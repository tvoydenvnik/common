<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 01.06.2016
 * Time: 14:53
 */

namespace Tvoydenvnik\Common;


class AppAnswer
{


    public $result = null;
    public $error = false;

    public $errorCode = "";

    public $errors = array();

    public $errorDescription = array();
    
    public function setResult($data){
        $this->result = $data;
        return $this;
    }

    public function getResult(){
        return $this->result;
    }

    public function setError($sDesc, $arData = null){
        $this->error = true;
        $newError = array("error"=>$sDesc);
        if($arData!==null){
            $newError["data"] = $arData;
        }
        
        array_push($this->errors, $newError);
        return $this;
    }

    public function setErrorCode($sCode){
        $this->error = true;
        $this->errorCode = $sCode;
        
        return $this;
    }
    
    public function hasError(){
        return $this->error;
    }

    public function setPublicErrorDesc($sDesc){
        $this->errorDescription = $sDesc;
        return $this;
    }
    
    public static function create(){
        return new AppAnswer();
    }
    
    
}